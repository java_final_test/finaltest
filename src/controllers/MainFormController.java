package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import java.io.File;

public class MainFormController {

    public MainFormController(){

    }

    @FXML
    private TextField txtFileRoot1;

    @FXML
    private TextField txtFileRoot2;

    /**
     Обработчик кнопки загрузки файла с тестовым набором
     */
    @FXML
    private void getFileChooser1(){
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(null);
        txtFileRoot1.setText(file.getAbsolutePath());
    }

    /**
    Обработчик кнопки загрузки файла с набором
     */
    @FXML
    private void getFileChooser2(){
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("IDX1-UBYTE","*.IDX1-UBYTE"));
        File file = chooser.showOpenDialog(null);
        try {
            txtFileRoot2.setText(file.getAbsolutePath());
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
