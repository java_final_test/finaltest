import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApplication extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Итоговое задание по курсу JAVA");

        try
        {
            FXMLLoader loader = new FXMLLoader();
            //Узказываем расположение главной формы приложения
            loader.setLocation(MainApplication.class.getResource("/views/MainForm.fxml"));
            Scene scene = new Scene(loader.load());
            //Устанавливаем иконку приложения
            primaryStage.getIcons().add(new Image("/appLogo.png"));
            //Запрещаем изменять размер окна приложения
            primaryStage.setResizable(false);
            primaryStage.setScene(scene);
            //Отрисовываем окно приложения
            primaryStage.show();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
